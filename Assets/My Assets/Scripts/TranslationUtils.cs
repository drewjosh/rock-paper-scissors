using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class TranslationUtils : MonoBehaviour
{
    /**
     * Returns string for a key from Localization table inclusive arguments for Smart Strings (works only with dictionary).
     * @source https://forum.unity.com/threads/localizating-strings-on-script.847000/#post-7106599
     */
    public static string Translate(string key, IList<object> args)
    {
        var op = LocalizationSettings.StringDatabase.GetLocalizedStringAsync("UI Translations", key, args);
        string translation = "";
        if (op.IsDone)
            translation = op.Result;
        else
            op.Completed += (o) => translation = o.Result;

        return translation;
    }
}