using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

/**
 * Controlls the rock paper scissors buttons according to current language.
 */
public class RPSButtonController : MonoBehaviour
{
    public Locale germanCH;
    public Locale englishGB;

    private Locale lastLocale;

    public GameObject swissButtons;
    public GameObject englishButtons;

    // Start is called before the first frame update
    void Start()
    {
        EnableButtonsAccordingToLanguage();
    }

    private void Update()
    {
        if (LocalizationSettings.SelectedLocale != lastLocale)
        {
            EnableButtonsAccordingToLanguage();
        }
    }

    /**
     * Enable buttons according to language.
     */
    public void EnableButtonsAccordingToLanguage()
    {
        if (LocalizationSettings.SelectedLocale == germanCH)
        {
            swissButtons.SetActive(true);
            englishButtons.SetActive(false);
        }
        else if (LocalizationSettings.SelectedLocale == englishGB)
        {
            swissButtons.SetActive(false);
            englishButtons.SetActive(true);
        }
        lastLocale = LocalizationSettings.SelectedLocale;
    }
}
