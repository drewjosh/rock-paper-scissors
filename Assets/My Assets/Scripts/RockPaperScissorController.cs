using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using static Weapon;

public class RockPaperScissorController : MonoBehaviour
{
    public GameMode mode; // in which game mode current game is

    public enum GameMode
    {
        Computer,
        LocalMultiplayer
    }

    public enum Player
    {
        A, // multiplayer a
        B, // multiplayer b
        Single // vs computer mode
    }

    // for both game modes
    public TextMeshProUGUI stateText;
    public GameObject newGameButton;

    // locales for language specific game
    public Locale germanCH;
    public Locale englishGB;

    // vs computer mode
    public GameObject computerModeView;
    public GameObject oponentsChoice;
    public Sprite rockSprite;
    public Sprite paperSprite;
    public Sprite scissorSprite;
    public GameObject playerControlls;
    WeaponType userChoice;
    WeaponType computerChoice;
    WeaponType[] computerChoices = { WeaponType.Rock, WeaponType.Paper, WeaponType.Scissors };
    int playerScore = 0;
    int computerScore = 0;
    public TextMeshProUGUI playerScoreText;
    public TextMeshProUGUI computerScoreText;

    // vs local player mode
    public GameObject localMultiPlayerView;
    GameObject currentPlayerAControls;
    GameObject currentPlayerBControls;
    public GameObject playerAEnglishControls;
    public GameObject playerASwissControls;
    public GameObject playerBEnglishControls;
    public GameObject playerBSwissControls;
    WeaponType playerAChoice;
    WeaponType playerBChoice;
    int playerAScore = 0;
    int playerBScore = 0;
    public TextMeshProUGUI playerALabel;
    public TextMeshProUGUI playerBLabel;
    public TextMeshProUGUI playerAScoreLabel;
    public TextMeshProUGUI playerBScoreLabel;
    int currentPlayer = 0;
    public GameObject menuButtonMultiplayer;

    // countdown stuff
    public float timeRemaining = 3;
    public bool timerIsRunning = false;
    // for time display: https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/


    private void Start()
    {
        ResetGame();
        SetCurrentPlayerControls();

        LocalizationSettings.SelectedLocaleChanged += (newLocale) =>
        {
            Debug.Log("locale changed, we need to update controls: " + newLocale);
            // update controls
            SetCurrentPlayerControls();
        };
    }

    private void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {

                timeRemaining -= Time.deltaTime;
                HandleGameAnimation(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 3;
                timerIsRunning = false;
                ShowMultiplayerChoice();
                DetermineWinner();
            }
        }
    }

    private void SetCurrentPlayerControls()
    {
        if (LocalizationSettings.SelectedLocale == germanCH)
        {
            currentPlayerAControls = playerASwissControls;
            currentPlayerBControls = playerBSwissControls;
            currentPlayerAControls.SetActive(true);
            currentPlayerBControls.SetActive(true);
            playerAEnglishControls.SetActive(false);
            playerBEnglishControls.SetActive(false);
        }
        else if (LocalizationSettings.SelectedLocale == englishGB)
        {
            currentPlayerAControls = playerAEnglishControls;
            currentPlayerBControls = playerBEnglishControls;
            currentPlayerAControls.SetActive(true);
            currentPlayerBControls.SetActive(true);
            playerASwissControls.SetActive(false);
            playerBSwissControls.SetActive(false);
        }
    }

    /**
     * Shows rock paper scissors animation before winner is shown.
     */
    private void HandleGameAnimation(float time)
    {
        time += 1;
        float seconds = Mathf.FloorToInt(time % 60);

        if (seconds == 3)
        {
            if (LocalizationSettings.SelectedLocale == germanCH)
            {
                stateText.text = TranslationUtils.Translate("SCISSORS", null);
            }
            else if (LocalizationSettings.SelectedLocale == englishGB)
            {
                stateText.text = TranslationUtils.Translate("ROCK", null);
            }
            stateText.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        else if (seconds == 2)
        {
            if (LocalizationSettings.SelectedLocale == germanCH)
            {
                stateText.text = TranslationUtils.Translate("ROCK", null);
            }
            else if (LocalizationSettings.SelectedLocale == englishGB)
            {
                stateText.text = TranslationUtils.Translate("PAPER", null);
            }
            stateText.transform.rotation = new Quaternion(0, 0, 180, 0);
        }
        else if (seconds == 1)
        {
            if (LocalizationSettings.SelectedLocale == germanCH)
            {
                stateText.text = TranslationUtils.Translate("PAPER", null) + "!";
            }
            else if (LocalizationSettings.SelectedLocale == englishGB)
            {
                stateText.text = TranslationUtils.Translate("SCISSORS", null) + "!";
            }
            stateText.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
    }

    public void StartGame()
    {
        ResetGame();
        Debug.Log("Start game: " + mode);

        if (mode == GameMode.LocalMultiplayer)
        {
            menuButtonMultiplayer.SetActive(false);
            playerAScore = 0;
            playerBScore = 0;
            computerModeView.SetActive(false);
            localMultiPlayerView.SetActive(true);
            ActivatePlayer(Player.A);
        }
        else if (mode == GameMode.Computer)
        {
            computerModeView.SetActive(true);
            localMultiPlayerView.SetActive(false);
            ActivatePlayer(Player.Single);
        }
    }

    public void ResetGame()
    {
        newGameButton.SetActive(false);
        if (mode == GameMode.Computer)
        {
            oponentsChoice.SetActive(false);
            var buttons = playerControlls.GetComponentsInChildren<Button>(true);
            foreach (Button button in buttons)
            {
                button.gameObject.SetActive(true);
            }
            stateText.text = "";
            ActivatePlayer(Player.Single);

        }
        else if (mode == GameMode.LocalMultiplayer)
        {
            var buttonsA = currentPlayerAControls.GetComponentsInChildren<Button>(true);
            foreach (Button button in buttonsA)
            {
                button.gameObject.SetActive(true);
            }

            var buttonsB = currentPlayerBControls.GetComponentsInChildren<Button>(true);
            foreach (Button button in buttonsB)
            {
                button.gameObject.SetActive(true);
            }
            playerBLabel.color = new Color(255, 255, 255);
            playerALabel.color = new Color(255, 255, 255);
            ActivatePlayer(Player.A);
            stateText.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
    }

    private void ComputerMakesChoice()
    {
        System.Random r = new System.Random();
        int randomIndex = r.Next(0, 3);
        computerChoice = computerChoices[randomIndex];

        if (computerChoice == WeaponType.Rock)
        {
            Debug.Log("computer picked rock");
            oponentsChoice.GetComponent<Image>().sprite = rockSprite;
        }
        else if (computerChoice == WeaponType.Paper)
        {
            Debug.Log("computer picked paper");
            oponentsChoice.GetComponent<Image>().sprite = paperSprite;
        }
        else if (computerChoice == WeaponType.Scissors)
        {
            Debug.Log("computer picked scissor");
            oponentsChoice.GetComponent<Image>().sprite = scissorSprite;

        }
        oponentsChoice.SetActive(true);

        DetermineWinner();
    }

    private void DetermineWinner()
    {
        if (mode == GameMode.Computer)
        {
            if (userChoice == computerChoice)
            {
                stateText.text = TranslationUtils.Translate("TIE", null);
                playerScore++;
                computerScore++;
            }
            else if (userChoice == WeaponType.Rock && computerChoice == WeaponType.Scissors
                || userChoice == WeaponType.Paper && computerChoice == WeaponType.Rock
                || userChoice == WeaponType.Scissors && computerChoice == WeaponType.Paper)
            {
                stateText.text = TranslationUtils.Translate("YOU_WIN", null);
                playerScore++;
            }
            else
            {
                stateText.text = TranslationUtils.Translate("YOU_LOOSE", null);
                computerScore++;
            }
            DeactivatePlayer(Player.Single);
            SetPvCScores();
        }
        else if (mode == GameMode.LocalMultiplayer)
        {
            if (playerAChoice == playerBChoice)
            {
                stateText.text = TranslationUtils.Translate("TIE", null);
                playerAScore++;
                playerBScore++;
            }
            else if (playerAChoice == WeaponType.Rock && playerBChoice == WeaponType.Scissors
              || playerAChoice == WeaponType.Paper && playerBChoice == WeaponType.Rock
              || playerAChoice == WeaponType.Scissors && playerBChoice == WeaponType.Paper)
            {
                // A wins!
                playerAScore++;
                stateText.text = TranslationUtils.Translate("PLAYER_1_WINS", null);
                stateText.transform.rotation = new Quaternion(0, 0, 0, 0);
            }
            else
            {
                // B wins!
                playerBScore++;
                stateText.text = TranslationUtils.Translate("PLAYER_2_WINS", null);
                stateText.transform.rotation = new Quaternion(0, 0, 180, 0);
            }
            SetPvPScores();
            menuButtonMultiplayer.SetActive(true);
        }
        newGameButton.SetActive(true);
    }

    /**
     * Sets scores in GameMode.Computer.
     * Smart strings set in scripts should be only handled in script.
     */
    private void SetPvCScores()
    {
        var dict1 = new Dictionary<string, string>() { { "score", playerScore + "" } };
        playerScoreText.text = TranslationUtils.Translate("YOUR_SCORE", new List<object> { dict1 });

        var dict2 = new Dictionary<string, string>() { { "score", computerScore + "" } };
        computerScoreText.text = TranslationUtils.Translate("OPPONENT_SCORE", new List<object> { dict2 });
    }

    /**
     * Sets scores in GameMode.LocalMultiplayer.
     * Smart strings set in scripts should be only handled in script.
     */
    private void SetPvPScores()
    {
        var dict1 = new Dictionary<string, int>() { { "score", playerAScore } };
        playerAScoreLabel.text = TranslationUtils.Translate("YOUR_SCORE", new List<object> { dict1 });

        var dict2 = new Dictionary<string, int>() { { "score", playerBScore } };
        playerBScoreLabel.text = TranslationUtils.Translate("YOUR_SCORE", new List<object> { dict2 });
    }

    /**
     * Only shows control that was clicked by player.
     */
    private void ShowMultiplayerChoice()
    {
        playerBLabel.color = new Color(8, 215, 4);
        playerALabel.color = new Color(8, 215, 4);

        // show choice of players
        if (playerAChoice == WeaponType.Rock)
        {
            SetActiveWeapon(WeaponType.Paper, false, Player.A);
            SetActiveWeapon(WeaponType.Scissors, false, Player.A);
        }
        else if (playerAChoice == WeaponType.Paper)
        {
            SetActiveWeapon(WeaponType.Rock, false, Player.A);
            SetActiveWeapon(WeaponType.Scissors, false, Player.A);
        }
        else if (playerAChoice == WeaponType.Scissors)
        {
            SetActiveWeapon(WeaponType.Rock, false, Player.A);
            SetActiveWeapon(WeaponType.Paper, false, Player.A);
        }

        if (playerBChoice == WeaponType.Rock)
        {
            SetActiveWeapon(WeaponType.Paper, false, Player.B);
            SetActiveWeapon(WeaponType.Scissors, false, Player.B);
        }
        else if (playerBChoice == WeaponType.Paper)
        {
            SetActiveWeapon(WeaponType.Rock, false, Player.B);
            SetActiveWeapon(WeaponType.Scissors, false, Player.B);
        }
        else if (playerBChoice == WeaponType.Scissors)
        {
            SetActiveWeapon(WeaponType.Rock, false, Player.B);
            SetActiveWeapon(WeaponType.Paper, false, Player.B);
        }
    }

    /**
     * Activate or deactivate specific weapon of a certain player.
     */
    private void SetActiveWeapon(WeaponType weapon, bool toActivate, Player player)
    {
        if (mode == GameMode.LocalMultiplayer)
        {
            if (player == Player.A)
            {
                var buttons = currentPlayerAControls.GetComponentsInChildren<Button>(true);
                var foundButton = Array.Find(buttons, button => button.gameObject.GetComponent<Weapon>().type == weapon);
                foundButton.gameObject.SetActive(toActivate);
            }

            if (player == Player.B)
            {
                var buttons = currentPlayerBControls.GetComponentsInChildren<Button>(true);
                var foundButton = Array.Find(buttons, button => button.gameObject.GetComponent<Weapon>().type == weapon);
                foundButton.gameObject.SetActive(toActivate);
            }
        }
        else if (mode == GameMode.Computer)
        {
            var buttons = playerControlls.GetComponentsInChildren<Button>(true);
            var foundButton = Array.Find(buttons, button => button.gameObject.GetComponent<Weapon>().type == weapon);
            foundButton.gameObject.SetActive(toActivate);
        }
    }

    /**
     * Click method for weapons.
     */
    public void ClickedWeapon(WeaponType choiceWeapon)
    {
        if (mode == GameMode.LocalMultiplayer)
        {
            if (currentPlayer == 1)
            {
                playerAChoice = choiceWeapon;
                DeactivatePlayer(Player.A);
                ActivatePlayer(Player.B);
            }
            else if (currentPlayer == 2)
            {
                playerBChoice = choiceWeapon;
                timerIsRunning = true;
                DeactivatePlayer(Player.B);
            }
        }
        else if (mode == GameMode.Computer)
        {
            userChoice = choiceWeapon;
            if (choiceWeapon == WeaponType.Rock)
            {
                Debug.Log("Clicked rock");
                SetActiveWeapon(WeaponType.Paper, false, Player.Single);
                SetActiveWeapon(WeaponType.Scissors, false, Player.Single);
            }
            else if (choiceWeapon == WeaponType.Paper)
            {
                Debug.Log("Clicked paper");
                SetActiveWeapon(WeaponType.Rock, false, Player.Single);
                SetActiveWeapon(WeaponType.Scissors, false, Player.Single);
            }
            else if (choiceWeapon == WeaponType.Scissors)
            {
                Debug.Log("Clicked scissors");
                SetActiveWeapon(WeaponType.Rock, false, Player.Single);
                SetActiveWeapon(WeaponType.Paper, false, Player.Single);
            }

            ComputerMakesChoice();
        }
    }

    /**
     * Activates a players control according to input and game mode.
     */
    private void ActivatePlayer(Player playerToActivate)
    {
        if (mode == GameMode.LocalMultiplayer)
        {
            if (playerToActivate == Player.A)
            {
                playerALabel.color = new Color(255, 186, 0);
                playerBLabel.color = new Color(255, 255, 255);
                SetInterceptableButtons(currentPlayerAControls, true);
                SetInterceptableButtons(currentPlayerBControls, false);
                currentPlayer = 1;
                stateText.text = TranslationUtils.Translate("PLAYER_1_TURN", null);
            }
            else if (playerToActivate == Player.B)
            {
                playerBLabel.color = new Color(255, 186, 0);
                playerALabel.color = new Color(255, 255, 255);
                SetInterceptableButtons(currentPlayerAControls, false);
                SetInterceptableButtons(currentPlayerBControls, true);
                currentPlayer = 2;
                stateText.text = TranslationUtils.Translate("PLAYER_2_TURN", null);
                stateText.transform.rotation = new Quaternion(0, 0, 180, 0);
            }
        }
        else if (mode == GameMode.Computer)
        {
            SetInterceptableButtons(playerControlls, true);
        }
    }

    /**
     * Deactivates a players control buttons.
     */
    private void DeactivatePlayer(Player player)
    {
        if (mode == GameMode.Computer)
        {
            SetInterceptableButtons(playerControlls, false);
        }
        else if (mode == GameMode.LocalMultiplayer)
        {
            if (player == Player.A)
            {
                SetInterceptableButtons(currentPlayerAControls, false);
            }
            else if (player == Player.B)
            {
                SetInterceptableButtons(currentPlayerBControls, false);
            }
        }
    }

    /**
     * Set interceptable true or false for given buttons in GameObject.
     */
    private void SetInterceptableButtons(GameObject parentOfButtons, bool isInterceptable)
    {
        Button[] buttons = parentOfButtons.GetComponentsInChildren<Button>(true);
        foreach (Button button in buttons)
        {
            button.interactable = isInterceptable;
        }
    }
}
