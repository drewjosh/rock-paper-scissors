using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveAppData()
    {
        string path = Application.persistentDataPath + "/appData.bla";

        BinaryFormatter formatter = new BinaryFormatter();

        FileStream stream = new FileStream(path, FileMode.Create);

        AppData data = new AppData();

        formatter.Serialize(stream, data);

        stream.Close();
    }

    public static AppData LoadAppData()
    {
        string path = Application.persistentDataPath + "/appData.bla";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            AppData data = formatter.Deserialize(stream) as AppData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.Log("Save File not found in " + path);
            return null;
        }
    }
}
