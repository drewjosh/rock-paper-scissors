using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class AppStartController : MonoBehaviour
{
    public Locale germanCH;
    public Locale englishGB;

    // Start is called before the first frame update
    void Awake()
    {
        InitAppLanguage();
    }

    private void InitAppLanguage()
    {
        AppData data = SaveSystem.LoadAppData();
        if (data == null)
        {
            // swiss german is default language
            LocalizationSettings.SelectedLocale = germanCH;
            SaveSystem.SaveAppData();
        }
        else
        {
            Debug.Log("Loaded language: " + data.appLanguage);
            if (data.appLanguage.Contains("de-CH"))
            {
                LocalizationSettings.SelectedLocale = germanCH;
            }
            else if (data.appLanguage.Contains("en-GB"))
            {
                LocalizationSettings.SelectedLocale = englishGB;
            }
            else
            {
                LocalizationSettings.SelectedLocale = germanCH;
            }
        }

    }
}
