using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UI;

public class MainMenueUI : MonoBehaviour
{
    public RockPaperScissorController gameController;

    public GameObject gameView;
    public GameObject mainMenueView;
    public GameObject roadmapContent;
    public GameObject gameModeButtons;
    public Image uiBackgroundImage;

    private void Start()
    {
        SetGameViewVisible(false);
        SetMainMenueViewVisible(true);
    }

    private void SetGameViewVisible(bool isVisible)
    {
        gameView.GetComponent<CanvasGroup>().alpha = isVisible ? 1 : 0;
        gameView.GetComponent<CanvasGroup>().blocksRaycasts = isVisible ? true : false;
    }

    private void SetMainMenueViewVisible(bool isVisible)
    {
        mainMenueView.GetComponent<CanvasGroup>().alpha = isVisible ? 1 : 0;
        mainMenueView.GetComponent<CanvasGroup>().blocksRaycasts = isVisible ? true : false;

        // set background because it would show blue background on notched phones in safe area
        uiBackgroundImage.enabled = isVisible;
    }

    public void StartGameAgainstComputer()
    {
        gameController.mode = RockPaperScissorController.GameMode.Computer;
        SetGameViewVisible(true);
        SetMainMenueViewVisible(false);
        gameController.StartGame();
    }

    public void StartLocalMultiplayer()
    {
        gameController.mode = RockPaperScissorController.GameMode.LocalMultiplayer;
        SetGameViewVisible(true);
        SetMainMenueViewVisible(false);
        gameController.StartGame();
    }

    public void ReturnToMenue()
    {
        SetGameViewVisible(false);
        SetMainMenueViewVisible(true);
    }

    /**
     * Toggle visibility of roadmap and game mode buttons.
     */
    public void ToggleRoadMap()
    {
        bool isActive = roadmapContent.activeSelf;
        roadmapContent.SetActive(!isActive);
        gameModeButtons.SetActive(isActive);
    }

    /**
     * Set given locale
     */
    public void SetLanguage(Locale aLanguage)
    {
        LocalizationSettings.SelectedLocale = aLanguage;
        SaveSystem.SaveAppData();
    }
}
