using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization.Settings;

[System.Serializable]
public class AppData
{
    public string appLanguage;

    public AppData()
    {
        appLanguage = LocalizationSettings.SelectedLocale.ToString();
    }
}
