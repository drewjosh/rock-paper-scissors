using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    public enum WeaponType
    {
        Rock,
        Paper,
        Scissors
    }

    public WeaponType type;
    RockPaperScissorController controller;

    private void Awake()
    {
        controller = GameObject.Find("RockPaperScissorController").GetComponent<RockPaperScissorController>();
        SetClickMethod();
    }

    /**
     * Setup click method for controls.
     */
    private void SetClickMethod()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(() => controller.ClickedWeapon(type));
    }
}
