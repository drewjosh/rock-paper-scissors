# Rock Paper Scissors

A rock paper scissors game with a Swiss touch

Project start: 30.6.2021

# Developers

- Joshua Drewlow

# Technologies

- Unity: 2021.3.x

# Deployment

- Android: [Google Play Store](https://play.google.com/store/apps/details?id=com.drewjoshcoding.SwissRockPaperScissors)
- iOS: [App Store](https://apps.apple.com/ch/app/swiss-rock-paper-scissors/id6444458307)

# Roadmap

- animations
- sound effects
- background soundtrack
- user must shake device to play
- improve UI
- nearby multiplayer (NFC/bluetooth/wifi)

# Changelog

| Version | Date       | Changes                                                                                                                                                                                        | Bugfixes                                                                         |
| ------- | ---------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| 1.2.0   | 07-12-2021 | - persist selected app language                                                                                                                                                                | - fixed layout issues regarding safe area                                        |
| 1.1.2   | 15-11-2021 | - added adaptive and legacy icons<br /> - improved splash screen                                                                                                                               | - multiplayer not starting correctly with default language                       |
| 1.1.1   | 15-11-2022 |                                                                                                                                                                                                | - show menu button after multiplayer game                                        |
| 1.1.0   | 15-11-2022 | - added local multiplayer mode<br /> - added translations for Swiss German<br /> - respect safe area of notched devices<br /> - added roadmap<br /> - upgraded from Unity 2020.3.x to 2021.3.x | - improvements for responsive UI<br />- deactivate all player buttons after game |
| 1.0.0   | 30-06-2021 | - minimal gameplay player vs computer<br /> - basic ui<br /> - score is not saved                                                                                                              |
